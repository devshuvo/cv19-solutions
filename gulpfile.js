// I don't feel like writing var everytime
var gulp = require("gulp"),
    sass = require("gulp-sass"),
    postcss = require("gulp-postcss"),
    autoprefixer = require("autoprefixer"),
    cssnano = require("cssnano"),
    sourcemaps = require("gulp-sourcemaps"),
    browsersync = require('browser-sync').create(),
    rename = require('gulp-rename');

// Put this after including our dependencies
var paths = {
    styles: {
      src: "src/frontend/scss/cv19-solutions-frontend.scss",
      dest: "assets/frontend/css"
    }
};

// Compile Styles
function style() {
    return (
      gulp.src(paths.styles.src)
      // Initialize sourcemaps before compilation starts
      .pipe(sourcemaps.init())
      .pipe(sass({ outputStyle: 'expanded' }))
      .on("error", sass.logError)      
      .pipe(postcss([autoprefixer()]))
      .pipe(gulp.dest(paths.styles.dest))
      .pipe(sass())
      .on("error", sass.logError)
      .pipe(postcss([autoprefixer(), cssnano()]))
      .pipe(rename({ suffix: '.min' }))    
      .pipe(sourcemaps.write())
      .pipe(gulp.dest(paths.styles.dest))      
    );
}

// Watching the file changes
function watch() {
  gulp.watch(
    ['src/*.scss', 'src/**/*.scss', '*.php', '**/*.php'],
    { events: 'all', ignoreInitial: false },
    gulp.series(style,function(done){
      browsersync.reload();
      done();
    })
  );
}

// Init BrowserSync.
function browserSync(done) { 

  browsersync.init(null, {
    proxy : "http://localhost/cv19/"
  });

  done();
}

exports.default = gulp.parallel(browserSync, watch); // $ gulp  
exports.watch = watch;