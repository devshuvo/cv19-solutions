<?php
/**
 * function cv19_custom_body_class
 * @param  array $classes
 * @return array
 */
function cv19_custom_body_class( $classes ) {
    //if ( is_page_template( 'templates/tpl-cv19-page.php' ) && ! is_user_logged_in() ) {
        $classes[] = 'logged_out_screen';
    //}

    return $classes;
}
add_filter( 'body_class', 'cv19_custom_body_class', 10, 1 );

/**
 * cv19_social_login_function
 * @return void
 */
function cv19_social_login_function() {

    echo do_shortcode( '[nextend_social_login]' );   
}
//add_filter( 'cv19_social_login', 'cv19_social_login_function' );

add_action( 'astra_page_template_parts_content', 'cv19_page_top_screen' );
if ( ! function_exists('cv19_page_top_screen') ) {
	function cv19_page_top_screen(){

		if( is_front_page() && !is_user_logged_in() ) : ?>
		<div class="cv19_logged_out_screen">
			<div class="signup-in-btngroup">
				<div class="row">					
					<div class="col-6">
						<div class="ast-article-single">
							<a href="<?php echo esc_url( home_url( '/register/' ) );?>" id="join_btn"><h2><?php echo esc_html__('Join', 'cv19-solutions'); ?></h2></a>
						</div>
					</div>
					<div class="col-6">
						<div class="ast-article-single">
							<a href="<?php echo esc_url( home_url( '/login/' ) );?>" id="login_btn"><h2><?php echo esc_html__('Login', 'cv19-solutions'); ?></h2></a>
						</div>
					</div>
				</div>
			</div>
		</div>		
		<?php endif;

	}
}