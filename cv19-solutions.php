<?php
/**
 * Plugin Name: CV19 Solutions
 * Plugin URI: https://bitbucket.org/devshuvo/cv19-solutions/
 * Bitbucket Plugin URI: https://bitbucket.org/devshuvo/cv19-solutions/
 * Description: CV19 Solutions mobile friendly plugin
 * Author: 
 * Author URI: 
 * Version: 1.0.1004
 * Text Domain: cv19-solutions
 * Domain Path: /lang
 * 
 */

/**
* Including Plugin file for security
* Include_once
* 
* @since 1.0.0
*/
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

/**
 *	Helper Function
 */
require_once( dirname( __FILE__ ) . '/inc/helper-functions.php' );

/**
 *	Page Templater Class
 */
require_once( dirname( __FILE__ ) . '/inc/class-pagetemplater.php' );

/**
 *	Page Templater Class
 */
require_once( dirname( __FILE__ ) . '/inc/tgm/suggested-plugins.php' );

/**
 *	Cv19Solutions Plugin Class
 */
if ( ! class_exists( 'Cv19Solutions' ) ) :
	class Cv19Solutions{

		public function __construct() {
			// Loaded textdomain
			add_action('plugins_loaded', array( $this, 'plugin_loaded_action' ), 10, 2);
			// Enqueue frontend scripts
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_frontend_script' ), 200 );			
			// Enqueue admin scripts
			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_script' ), 100 );	

			// Hook the templates
			add_filter( 'single_template', [ $this, 'cv19_single_page_template' ] );
			add_filter('template_include', [ $this, 'cv19_archive_page_template' ]);

			add_action( 'widgets_init', array( $this, 'sidebar_registration'));
		}	 	

		/**
		* Loading Text Domain for Internationalization
		*
		*  @return void
		*  
		*/
		function plugin_loaded_action() {
			load_plugin_textdomain( 'cv19-solutions', false, dirname( plugin_basename(__FILE__) ) . '/lang/' );
		}	

		/**
		 * Enqueue frontend script
		 *
		 *  @return void
		 *
		 */
		function enqueue_frontend_script() {

			$ver = current_time('timestamp');

			wp_enqueue_style( 'material-icons', 'https://fonts.googleapis.com/icon?family=Material+Icons' );
		    wp_enqueue_style( 'cv19-solutions-frontend', plugin_dir_url( __FILE__ ) . 'assets/frontend/css/cv19-solutions-frontend.min.css', array(), $ver );

		    wp_enqueue_script( 'cv19-solutions-frontend', plugin_dir_url( __FILE__ ) . 'assets/frontend/js/cv19-solutions-frontend.js', array('jquery'), $ver );
		}

		/**
		 * Enqueue aadmin script
		 *
		 * @return void
		 *
		 */
		function enqueue_admin_script( $hook ) {	

		    wp_enqueue_style( 'cv19-solutions-admin', plugin_dir_url( __FILE__ ) . 'assets/admin/css/cv19-solutions-admin.min.css' );

		    wp_enqueue_script( 'cv19-solutions-admin', plugin_dir_url( __FILE__ ) . 'assets/admin/js/cv19-solutions-admin.js', array('jquery'), '1.0' );
		}

		/**
		 * Load Single Template
		 * 
		 * @param  string $single_template
		 * 
		 * @return $single_template
		 */
		public function cv19_single_page_template( $single_template ) {
			global $post;
			if ( 'cvfeed' === $post->post_type ) {
			    $theme_files = array('single-cvfeed.php', 'templates/single-cvfeed.php');
			    $exists_in_theme = locate_template($theme_files, false);
			    if ( $exists_in_theme != '' ) {
			      	return $exists_in_theme;
			    } else {
			      	return dirname( __FILE__ ) . '/templates/single-cvfeed.php';
			    }
			}
			return $single_template;
		}

		/**
		 * Load Archive Template
		 * 
		 * @param  string $template
		 * 
		 * @return $template
		 */
		public function cv19_archive_page_template( $template ) {
		  if ( is_post_type_archive('cvfeed') ) {

		    $theme_files = array('archive-cvfeed.php', 'templates/archive-cvfeed.php');
		    $exists_in_theme = locate_template($theme_files, false);
		    if ( $exists_in_theme != '' ) {
		      	return $exists_in_theme;
		    } else {
		      	return dirname( __FILE__ ) . '/templates/archive-cvfeed.php';
		    }

		  }
		  return $template;
		}

		public function sidebar_registration() {

			// Arguments used in all register_sidebar() calls.
			$shared_args = array(
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
				'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
				'after_widget'  => '</div></div>',
			);

			// Sidebar #1.
			register_sidebar(
				array_merge(
					$shared_args,
					array(
						'name'        => __( 'BP Login Page', 'addomas' ),
						'id'          => 'bp_login_sidebar',
						'description' => __( 'Widgets in this area will be displayed in footer.', 'addomas' ),
					)
				)
			);
		}

	}

endif;

new Cv19Solutions();
